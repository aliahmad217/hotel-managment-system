namespace HMS.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BasicEntities1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccomodationsPackages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccomodationTypeId = c.Int(nullable: false),
                        Name = c.String(),
                        NoOfRoom = c.Int(nullable: false),
                        FeePerNight = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccomodationTypes", t => t.AccomodationTypeId, cascadeDelete: true)
                .Index(t => t.AccomodationTypeId);
            
            CreateTable(
                "dbo.Accomodations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccomodationPackageId = c.Int(nullable: false),
                        Name = c.String(),
                        AccomodationsPackage_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccomodationsPackages", t => t.AccomodationsPackage_Id)
                .Index(t => t.AccomodationsPackage_Id);
            
            CreateTable(
                "dbo.Bookings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AcoomodationId = c.Int(nullable: false),
                        FromDate = c.DateTime(nullable: false),
                        Duration = c.Int(nullable: false),
                        Accomodation_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accomodations", t => t.Accomodation_Id)
                .Index(t => t.Accomodation_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bookings", "Accomodation_Id", "dbo.Accomodations");
            DropForeignKey("dbo.Accomodations", "AccomodationsPackage_Id", "dbo.AccomodationsPackages");
            DropForeignKey("dbo.AccomodationsPackages", "AccomodationTypeId", "dbo.AccomodationTypes");
            DropIndex("dbo.Bookings", new[] { "Accomodation_Id" });
            DropIndex("dbo.Accomodations", new[] { "AccomodationsPackage_Id" });
            DropIndex("dbo.AccomodationsPackages", new[] { "AccomodationTypeId" });
            DropTable("dbo.Bookings");
            DropTable("dbo.Accomodations");
            DropTable("dbo.AccomodationsPackages");
        }
    }
}
